import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { DealerService } from '../dealer.service';

@Component({
  selector: 'dealer-details',
  templateUrl: './dealer-details.component.html',
  styleUrls: ['./dealer-details.component.css']
})
export class DealerDetailsComponent implements OnInit {
  
  selectedDealer = {};
  Dealer = [];
  Id = null;
  Dname = '';
  Daddress = ''; 
  Dphone = null;
  Agency_name = ''; 
  Dusername = '';
  Dpassword = ''; 
  Age = null;
  Gender = '';
  Demail = '';
  Dthumbnail = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dealerService: DealerService) { 

      this.refreshProductList(); 
        this.activatedRoute.queryParams.subscribe(params => {
          const Id = params['Id'];

          this.dealerService
              .getDealerDetails(Id)
              .subscribe(response => {
                const result = response.json();
                this.selectedDealer = result.data;
              });
        });
  }

  refreshProductList(){
    this.dealerService
        .getDealer()
        .subscribe(response => {
          const result = response.json();
          console.log(result);
          this.Dealer=result.data;
        });
  }

  onCancel(){
    return this.router.navigate(['/dealer-list']);
  }

  ngOnInit() {
  }

}
