import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {
 
  Cname = ''; 
  Carea = '';
  Pin = null;
  Phone = '';
  Cemail = '';
  Cusername = '';
  Cpassword = '';
  Cgender = '';
  imageUrl: any;
  selectedFile : any;
  constructor(
    private router: Router,
    private customerService: CustomerService
  ) { }

  onAdd(){
      this.customerService
          .addCustomers(this.Cname, this.Carea,this.Pin,
            this.Phone,this.Cemail,this.Cusername,
            this.Cpassword,this.Cgender,this.selectedFile)
          .subscribe( response =>{
              console.log(response);
              const body = response.json();
              if (body['status'] == 'success') {
                // go to the list of product
                this.router.navigate(['/customer-list']);
              } else {
                alert('error while adding customer');
              }
          });       
  }

  onCancel(){
    return this.router.navigate(['/customer-list']);
  }

  ngOnInit() {
  }

  onChange($event) {
    this.selectedFile = $event.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.imageUrl = fileReader.result;
    };
    fileReader.readAsDataURL(this.selectedFile);
  }

}
