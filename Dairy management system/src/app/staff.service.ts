import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class StaffService{

    url = 'http://localhost:9000/staff';
    
    constructor(private http: Http){
        
    }

    getStaffList(){
        return this.http.get(this.url);
    }

    addStaff(
        Sname:string, Saddress: string,
        Semail:string, DeptId: number, Srole: string,
        Ssalary:number, Susername: string, Spassword: string,
        Sgender: string, Sage: number, Sphone: string,Sthumbnail: string
    ){
        const body = {
            Sname : Sname, Saddress: Saddress,
            Semail:Semail, DeptId: DeptId, Srole: Srole,
            Ssalary:Ssalary, Susername: Susername, Spassword: Spassword,
            Sgender: Sgender, Sage: Sage, Sphone: Sphone,Sthumbnail: Sthumbnail
        } 

        const header =new Headers({'Content-Type' : 'application/json'});
        const requestOption =new RequestOptions({ headers : header});

        return this.http.post( this.url , body, requestOption);
    }

    deleteStaff(Sid: number){
        return this.http.delete(this.url + '/' + Sid);
    }

    updateStaff(
        Sname: string, Saddress: string,
        Semail: string, DeptId: number, Srole: string,
        Ssalary: number, Susername: string, Spassword: string,
        Sgender: string, Sage: number, Sphone: string, Sthumbnail: string
    ){
        const body = {
            Sname: Sname, Saddress: Saddress,
            Semail: Semail, DeptId: DeptId, Srole: Srole,
            Ssalary: Ssalary, Susername: Susername, Spassword: Spassword,
            Sgender: Sgender, Sage: Sage, Sphone: Sphone, Sthumbnail: Sthumbnail
        }

        const header = new Headers({'Content-Type': 'application/json'});
        const requestOption = new RequestOptions({ headers : header });

        return this.http.put( this.url, body ,requestOption );
    }

    getStaffDetails(Sid: number){
        return this.http.get(this.url + '/' + Sid);
    }
}