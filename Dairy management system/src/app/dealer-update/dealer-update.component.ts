import { Component, OnInit } from '@angular/core';
import { DealerService } from '../dealer.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'dealer-update',
  templateUrl: './dealer-update.component.html',
  styleUrls: ['./dealer-update.component.css']
})
export class DealerUpdateComponent implements OnInit {
  selectedDealer = {};
  Dealers = [];
  Did = null;
  Dname = '';
  Daddress = ''; 
  Dphone = null;
  Agency_name = ''; 
  Dusername = '';
  Dpassword = ''; 
  Age = null;
  Gender = '';
  Demail = '';
  Dthumbnail = '';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dealerService: DealerService) {

      this.activatedRoute.queryParams.subscribe(params => {
        const Did = params['Did'];
  
        this.dealerService
          .getDealerDetails(Did)
          .subscribe(response => {
            const result = response.json();
            //console.log(result);
            this.selectedDealer=result.data;
          });
      });
     }

  ngOnInit() {
  }

  onUpdate(){
    this.dealerService
        .updateDealer(this.Dname,this.Daddress,this.Dphone,this.Agency_name,this.Dusername,this.Dpassword,this.Age,this.Gender,this.Demail,this.Dthumbnail)
        .subscribe(response =>{
          console.log(response);
          this.router.navigate(['/dealer-list']);
        });
  }

  onCancel(){
    this.router.navigate(['/dealer-list']);
  }

}
