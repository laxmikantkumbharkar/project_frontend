import { BrowserModule } from '@angular/platform-browser';
import { NgModule,Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, CheckboxControlValueAccessor } from '@angular/forms';
import { AppComponent } from './app.component';

import { ProductService } from './product.service';
import { MilkRateService } from './milkrate.service';
import { CustomerService } from './customer.service';
import { DealerService } from './dealer.service';
import { StaffService } from './staff.service';
import { FeedbackService } from './feedback.service';
import { LoginService } from './login.service';
//Product realted component
import { ProductListComponent } from './product-list/product-list.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { UpdateProductComponent } from './update-product/update-product.component';

import { LoginComponent } from './login/login.component';
//Milk rate related component
import { MilkRateListComponent } from './milk-rate-list/milk-rate-list.component';
import { AddMilkRateComponent } from './add-milk-rate/add-milk-rate.component';
//Customer realted component
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
//Dealer realted component
import { DealerListComponent } from './dealer-list/dealer-list.component';
import { AddDealerComponent } from './add-dealer/add-dealer.component';
//Staff realted component
import { StaffListComponent } from './staff-list/staff-list.component';
import { AddStaffComponent } from './add-staff/add-staff.component';
//Feedback realted component
import { FeedbackListComponent } from './feedback-list/feedback-list.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { HomepageComponent } from './homepage/homepage.component';
import { UpdateCustomerComponent } from './update-customer/update-customer.component';
import { DealerDetailsComponent } from './dealer-details/dealer-details.component';
import { StaffDetailsComponent } from './staff-details/staff-details.component';
import { DealerUpdateComponent } from './dealer-update/dealer-update.component';
import { StaffUpdateComponent } from './staff-update/staff-update.component';
import { FeedbackReplyComponent } from './feedback-reply/feedback-reply.component';
import { FeedbackAddComponent } from './feedback-add/feedback-add.component';
import { FeedbackDetailsComponent } from './feedback-details/feedback-details.component';
import { longStackSupport } from 'q';

// decorator
// metadata
@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    AddProductComponent,
    UpdateProductComponent,
    MilkRateListComponent,
    LoginComponent,
    AddMilkRateComponent,
    CustomerListComponent,
    AddCustomerComponent,
    DealerListComponent,
    AddDealerComponent,
    StaffListComponent,
    AddStaffComponent,
    FeedbackListComponent,
    ProductDetailsComponent,
    CustomerDetailsComponent,
    HomepageComponent,
    UpdateCustomerComponent,
    DealerDetailsComponent,
    StaffDetailsComponent,
    DealerUpdateComponent,
    StaffUpdateComponent,
    FeedbackReplyComponent,
    FeedbackAddComponent,
    FeedbackDetailsComponent,
   
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {
    path: '',
    redirectTo: "/homepage",
    pathMatch: 'full' 
      },
      { path : 'homepage' ,component:HomepageComponent},
      //Product related routers
      { path : 'product-list' ,component:ProductListComponent,canActivate: [LoginService]},
      { path : 'add-product' , component:AddProductComponent,canActivate: [LoginService]},
      { path : 'update-product' , component:UpdateProductComponent,canActivate: [LoginService]},
      { path : 'product-details' ,component:ProductDetailsComponent,canActivate: [LoginService]},
      //Milk rate related routers
      { path : 'milk-rate-list' , component:MilkRateListComponent,canActivate: [LoginService]},
      { path : 'add-milk-rate' , component:AddMilkRateComponent,canActivate: [LoginService]},
      //Login page related routers
      { path : 'login' , component: LoginComponent},
      //Customer related routers
      { path : 'customer-list' , component: CustomerListComponent,canActivate: [LoginService]},
      { path : 'add-customer' , component: AddCustomerComponent,canActivate: [LoginService]},
      { path : 'customer-details' , component: CustomerDetailsComponent,canActivate: [LoginService]},
      { path : 'update-customer' , component: UpdateCustomerComponent,canActivate: [LoginService]},
      //Dealer related routers
      { path : 'dealer-list' , component: DealerListComponent,canActivate: [LoginService]},
      { path : 'add-dealer' , component: AddDealerComponent,canActivate: [LoginService]},
      { path : 'dealer-details' , component: DealerDetailsComponent,canActivate: [LoginService]},
      { path : 'dealer-update' , component: DealerUpdateComponent,canActivate: [LoginService]},
      //Staff related routers
      { path : 'staff-list' , component: StaffListComponent,canActivate: [LoginService]},
      { path : 'add-staff' , component: AddStaffComponent,canActivate: [LoginService]},
      { path : 'staff-details' , component: StaffDetailsComponent,canActivate: [LoginService]},
      { path : 'staff-update' , component: StaffUpdateComponent,canActivate: [LoginService]},
      //Feedback related routers
      { path : 'feedback-list' , component: FeedbackListComponent,canActivate: [LoginService]},
      { path : 'feedback-reply' , component: FeedbackReplyComponent,canActivate: [LoginService]},
      { path : 'feedback-add' , component: FeedbackAddComponent,canActivate: [LoginService]},
      { path : 'feedback-details' , component: FeedbackDetailsComponent,canActivate: [LoginService]},
      
      //Login related routers
      { path : 'login' , component: LoginComponent ,canActivate: [LoginService],
      // children: [
      //   { path : 'feedback-list' , component: FeedbackListComponent,canActivate: [LoginService]},
      // { path : 'feedback-reply' , component: FeedbackReplyComponent,canActivate: [LoginService]},
      // { path : 'feedback-add' , component: FeedbackAddComponent,canActivate: [LoginService]},
      // { path : 'feedback-details' , component: FeedbackDetailsComponent,canActivate: [LoginService]}
      // ]
    },
    ])
  ],
  providers: [
    ProductService,
    MilkRateService,
    CustomerService,
    DealerService,
    StaffService,
    FeedbackService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


