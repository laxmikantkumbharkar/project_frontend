import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { StaffService } from '../staff.service';

@Component({
  selector: 'staff-details',
  templateUrl: './staff-details.component.html',
  styleUrls: ['./staff-details.component.css']
})
export class StaffDetailsComponent implements OnInit {

  selectedStaff = {};
  Staff = [];               Id = null; 
  Sname = '';               Saddress = '';
  Semail = '';              DeptId = null;  
  Srole = '';               Ssalary = null; 
  Susername = '';           Spassword = '';
  Sgender = '';             Sage = null;  
  Sphone = '';              Sthumbnail = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private staffService: StaffService) { 
        this.refreshStaffList(); 
            this.activatedRoute.queryParams.subscribe(params => {
              const Id = params['Id'];

              this.staffService
                  .getStaffDetails(Id)
                  .subscribe(response => {
                    const result = response.json();
                    this.selectedStaff = result.data;
                  });
            });
  }

  refreshStaffList(){
    this.staffService
        .getStaffList()
        .subscribe(response => {
          const result = response.json();
          this.selectedStaff = result.data;
        });
  }

  onCancel(){
    return this.router.navigate(['/staff-list']);
  }

  ngOnInit() {
  }

}
