import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  selectedProduct = {};   
  Products = [];   //db table name
  Pname = '';
  Prate = null;
  Pcategory = '';
  PQuantity = '';
  Pthumbnail = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService ) { 
      this.refreshProductList();
            this.activatedRoute.queryParams.subscribe(params => {
              const Pid = params['Pid'];

              this.productService
                .getProductDetails(Pid)
                .subscribe(response => {
                  const result = response.json();
                  //console.log(result);
                  this.selectedProduct=result.data;
                });
            }); 
  }

  refreshProductList(){
    this.productService
        .getProducts()
        .subscribe(response => {
          const result = response.json();
          console.log(result);
          this.Products=result.data;
        });
  }

  ngOnInit() {
  }

}
