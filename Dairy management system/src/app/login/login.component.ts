import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  Susername = '';
  Spassword = '';
  constructor(
    private router: Router,
    private loginService: LoginService) { }

  ngOnInit() {
  }

  onLogin(){
    this.loginService
        .signin(this.Susername,this.Spassword)
        .subscribe(response =>{
          const result = response.json();
          if(this.Susername == ""){
            alert('invalid username or password')
          }
          else if(result.status == 'error'){
            alert('invalid username or password')
          }else{
            sessionStorage['login_status'] = '1';
            alert('Welcome to product list page');
            this.router.navigate(['/product-list'])
          }
        });
  }

  onCancel(){
    this.router.navigate(['/product-list']);
  }

}
