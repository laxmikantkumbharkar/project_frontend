import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  selectedProduct = {};
  Products = []
  Pid = null;
  Pname = '';
  Prate = null;
  Pcategory = '';
  PQuantity = null;
  Pthumbnail = '';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private productService: ProductService) { 
      this.activatedRoute.queryParams.subscribe(params => {
        const Pid = params['Pid'];

        this.productService
          .getProductDetails(Pid)
          .subscribe(response => {
            const result = response.json();
            //console.log(result);
            this.selectedProduct=result.data;
          });
      }); 
    }

  ngOnInit() {
  }

  onUpdate(Pid){
    this.activatedRoute.queryParams.subscribe(params => {
      //const Pid = params['Pid'];
    //const Pid = params['Pid'];
    //this.Pname=Products.Pname;
    
    this.productService
        .updateProducts(Pid,this.Pname,this.Prate,this.Pcategory,this.PQuantity)
        .subscribe(response =>{
          const body = response.json();
          console.log(body);
          this.router.navigate(['/product-list']);
        });
      });
  }

  onCancel(){
    this.router.navigate(['/product-list']);
  }

}
