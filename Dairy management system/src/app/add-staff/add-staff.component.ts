import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StaffService } from '../staff.service';

@Component({
  selector: 'app-add-staff',
  templateUrl: './add-staff.component.html',
  styleUrls: ['./add-staff.component.css']
})
export class AddStaffComponent implements OnInit {

  Sname = '';
  Saddress = '';
  Semail = ''; 
  DeptId = null; 
  Srole = '';
  Ssalary = null; 
  Susername = ''; 
  Spassword = '';
  Sgender = ''; 
  Sage = null; 
  Sphone = '';
  Sthumbnail = '';

  constructor(
    private router: Router,
    private staffService: StaffService ) {   }

    onAdd(){
      this.staffService
          .addStaff(this.Sname, this.Saddress,
                    this.Semail, this.DeptId, this.Srole,
                    this.Ssalary, this.Susername, this.Spassword,
                    this.Sgender, this.Sage, this.Sphone,this.Sthumbnail)
          .subscribe( response => {
              console.log(response);

              this.router.navigate(['/staff-list']);
          });
    }

    onCancel(){
        this.router.navigate(['/staff-list']);
    }

  ngOnInit() {
  }

}
