import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductService {

  url = 'http://localhost:9000/product';

  Pid:number;
  constructor(private http: Http) {

  }

  //Products Services
  //================================================================
  getProducts(){
    return this.http.get(this.url);
  }

  getProductDetails(Pid:number){
    console.log(this.url+ '/' + Pid);
    return this.http.get(this.url + '/'+ Pid);
  }

  searchProducts(text:string){
    console.log(this.url+ '/' + text);
    return this.http.get(this.url + '/search/' + text);
  }

  addProducts(
    Pname:string,Prate:number,Pcategory:string,PQuantity:string,selectedFile: any){
    
      const body= new FormData();
        body.append('Pname',Pname);
        body.append('Prate',''+ Prate);
        body.append('Pcategory',Pcategory);
        body.append('PQuantity',PQuantity);
        body.append('Pthumbnail',selectedFile);

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });
      return this.http.post(this.url, body,requestOption);
  }

  deleteProducts(Pid:number){
    return this.http.delete(this.url + '/' + Pid);
  }
  
  updateProducts(
    Pid:number,Pname:string,Prate:number,Pcategory:string,PQuantity:number){
      //const Pid = params['Pid']; 
      //const Pid=null;
      const Products = [];
      const body={
        //Pid:Pid,
        Pname:Pname,
        Prate:Prate,
        Pcategory:Pcategory,
        PQuantity:PQuantity,
        // Pthumbnail:Pthumbnail
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });
      return this.http.put(this.url+'/'+Pid, body, requestOption);
  }

  getSortedByNameProducts(){
    return this.http.get(this.url+ '/sort/name');
  }

  getSortedPriceProducts(){
    return this.http.get(this.url+ '/sort/rate');
  }

  getSortedByCategoryProducts(){
    return this.http.get(this.url+ '/sort/Pcategory');
  }

  getButterProducts(){
    return this.http.get(this.url+ '/sort/butter');
  }

  getPaneerProducts(){
    return this.http.get(this.url+ '/sort/paneer');
  }

  getMilkProducts(){
    return this.http.get(this.url+ '/sort/milk');
  }

  getIcecreamProducts(){
    return this.http.get(this.url+ '/sort/icecream');
  }

  getGheeProducts(){
    return this.http.get(this.url+ '/sort/ghee');
  }

}