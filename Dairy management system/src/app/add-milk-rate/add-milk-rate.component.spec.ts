import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMilkRateComponent } from './add-milk-rate.component';

describe('AddMilkRateComponent', () => {
  let component: AddMilkRateComponent;
  let fixture: ComponentFixture<AddMilkRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMilkRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMilkRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
