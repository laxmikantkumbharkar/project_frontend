import { Component, OnInit } from '@angular/core';
import { MilkRateService} from '../milkrate.service';
import { Router } from '@angular/router'
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'add-milk-rate',
  templateUrl: './add-milk-rate.component.html',
  styleUrls: ['./add-milk-rate.component.css']
})
export class AddMilkRateComponent implements OnInit {
  
  Fat = null;
  Buffalo = null;
  Cow = null;
  constructor(
    private router : Router,
    private milkrateService: MilkRateService ) { }

  ngOnInit() {
  }

  onAdd(){
    this.milkrateService
      .addMilkrate(this.Fat, this.Buffalo , this.Cow )
      .subscribe(response =>{
          console.log(response);
          this.router.navigate(["/milk-rate-list"])
      });
  }

  onCancel(){
    this.router.navigate(["/milk-rate-list"]);
  }

}
