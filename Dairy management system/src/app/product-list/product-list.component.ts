import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router'

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  Products = [];   //db table name
  Pname = '';
  Prate = null;
  Pcategory = '';
  PQuantity = '';
  Pthumbnail = '';

  constructor(
    private router: Router,
    private productService: ProductService) {
      this.refreshProductList();
     }

     refreshProductList(){
       this.productService
           .getProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
     }

     onSearch(product){
      this.productService.searchProducts(product.text)
          .subscribe(response => {
            const result = response.json();
            console.log(result);
            this.Products = result.data;
          });
    }

     onDetails(product){
       this.router.navigate(['/product-details'],{ queryParams: {Pid:product.Pid}});
     }

     onDelete(product){
        const answer = confirm('Are you sure want to delete '+product.Pname+ ' ?');
        if(answer){
          this.productService.deleteProducts(product.Pid)
              .subscribe(response => {
                const result = response.json();
                console.log(result);
                this.refreshProductList();
              });
        }
    }

    onUpdate(product){
      this.router.navigate(['/update-product'],{ queryParams: {Pid:product.Pid}});
    };

    onSortPrice(product){
      this.productService
           .getSortedPriceProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

    onSortList(product){
      this.productService
           .getSortedByNameProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

    onSortCategory(product){
      this.productService
           .getSortedByCategoryProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

    onSortByButterCategory(product){
      this.productService
           .getButterProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

    onSortByPaneerCategory(product){
      this.productService
           .getPaneerProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

    onSortByMilkCategory(product){
      this.productService
           .getMilkProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

    onSortByIcecreamCategory(){
      this.productService
           .getIcecreamProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

    onSortByGheeCategory(product){
      this.productService
           .getGheeProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
    }

  ngOnInit() {
  }

  }

