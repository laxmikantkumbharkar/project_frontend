import { Injectable } from '@angular/core';
import { Http,RequestOptions, Headers} from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';

@Injectable()
export class LoginService implements CanActivate{

    url = 'http://localhost:9000/staff';

    constructor(
        private router: Router,
        private http: Http){ }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if(sessionStorage['login_status'] == '1'){
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }

    signup(){

    }

    signin(Susername: string, Spassword: string){
        const body = {
            Susername: Susername,
            Spassword: Spassword
        };

        const header = new Headers({'Content-Type':'application/json'});
        const requestOption = new RequestOptions({headers: header});

        return this.http.post(this.url + '/signin',body,requestOption);
    }

    logout(){
        sessionStorage.clear();
        this.router.navigate(['/login']);
      }
      
    Checkuser(){
    if (sessionStorage.getItem('login_status') == '1'){
        return true
    }else{
        return false
    }
    }
}