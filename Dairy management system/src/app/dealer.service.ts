import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';


@Injectable()
export class DealerService{
    
    url = 'http://localhost:9000/dealer';

    constructor(private http: Http){

    }

    getDealer(){
        return this.http.get(this.url);
    }

    addDealer(
        Dname:string, Daddress: string, Dphone: string,
        Agency_name: string, Dusername: string,
        Dpassword: string, Age: number, Gender: string,
        Demail: string,Dthumnail: string
    ){
        const body = {
            Dname: Dname, Daddress: Daddress,
            Dphone: Dphone, Agency_name: Agency_name,
            Dusername: Dusername, Dpassword: Dpassword,
            Age: Age, Gender: Gender, Demail: Demail,Dthumnail: Dthumnail
        };

        const header = new Headers ({'Content-Type': 'application/json'});
        const requestOption = new RequestOptions({headers: header});
        return this.http.post(this.url,body,requestOption);
    }

    updateDealer(
        Dname:string, Daddress: string, Dphone: string,
        Agency_name: string, Dusername: string,
        Dpassword: string, Age: number, Gender: string,
        Demail: string, Dthumnail: string
    ){
        const body = {
            Dname: Dname, Daddress: Daddress,
            Dphone: Dphone, Agency_name: Agency_name,
            Dusername: Dusername, Dpassword: Dpassword,
            Age: Age, Gender: Gender, Demail: Demail, Dthumnail: Dthumnail 
        };

        const header = new Headers ({'Content-Type': 'application/json'});
        const requestOption = new RequestOptions({headers: header});
        return this.http.put(this.url,body,requestOption);
    }

    deleteDealer(Id: number){
       return this.http.delete(this.url + '/' + Id);
    }

    getDealerDetails(Id: number){
       return this.http.get(this.url + '/' + Id);
    }
}