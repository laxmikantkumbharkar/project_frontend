import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'update-customer',
  templateUrl: './update-customer.component.html',
  styleUrls: ['./update-customer.component.css']
})
export class UpdateCustomerComponent implements OnInit {
  selectedCustomer = {};
  Customer = [];
  Cid = null;
  Cname = ''; 
  Carea = '';
  Pin = null;
  Phone = null;
  Cemail = '';
  Cusername = '';
  Cpassword = '';
  Cgender = '';
  Cthumbnail = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService
  ) { 
    this.activatedRoute.queryParams.subscribe(params => {
      const Cid = params['Cid'];

      this.customerService
        .getCustomerDetails(Cid)
        .subscribe(response => {
          const result = response.json();
          //console.log(result);
          this.selectedCustomer=result.data;
        });
    });
  }

  ngOnInit() {
  }

  onUpdate(){
    this.customerService
        .updateCustomer(this.Cname,this.Carea,this.Pin,this.Phone,this.Cemail,this.Cusername,this.Cpassword,this.Cgender,this.Cthumbnail)
        .subscribe(response =>{
          console.log(response);
          this.router.navigate(['/customer-list']);
        });
  }

  onCancel(){
    this.router.navigate(['/customer-list']);
  }

}
