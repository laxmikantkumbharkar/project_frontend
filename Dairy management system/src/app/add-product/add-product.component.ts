import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  Pname = '';
  Prate = null;
  Pcategory = '';
  PQuantity = '';
  imageUrl: any;
  selectedFile : any;
  constructor(
    private router: Router,
    private productService: ProductService)
    { }

  ngOnInit() {
  }

  onAdd(){
    this.productService
        .addProducts(this.Pname,this.Prate,this.Pcategory,this.PQuantity,this.selectedFile)
        .subscribe(response =>{
          console.log(response);
          const body = response.json();
        if (body['status'] == 'success') {
          // go to the list of product
          this.router.navigate(['/product-list']);
        } else {
          alert('error while adding product');
        }
        });
  }

  onCancel(){
    this.router.navigate(['/product-list']);
  }

  onChange($event) {
    this.selectedFile = $event.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.imageUrl = fileReader.result;
    };
    fileReader.readAsDataURL(this.selectedFile);
  }

}
