import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackReplyComponent } from './feedback-reply.component';

describe('FeedbackReplyComponent', () => {
  let component: FeedbackReplyComponent;
  let fixture: ComponentFixture<FeedbackReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackReplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
