import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { FeedbackService } from '../feedback.service';

@Component({
  selector: 'feedback-reply',
  templateUrl: './feedback-reply.component.html',
  styleUrls: ['./feedback-reply.component.css']
})
export class FeedbackReplyComponent implements OnInit {

  selectedFeedback = {};
  Feedback = [];
  fb = '';
  Response = '';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private feedbackService: FeedbackService  ) {
      this.activatedRoute.queryParams.subscribe(params => {
        const Name = params['Name'];
      
        this.feedbackService
          .getFeedbackDetails(Name)
          .subscribe(response => {
            const result = response.json();
            //console.log(result);
            this.selectedFeedback=result.data;
          });
      });
     }

     onUpdate(){
      this.feedbackService
          .updateFeedback(this.Response)
          .subscribe(response =>{
            console.log(response);
            this.router.navigate(['/feedback-list']);
          });
    }
  
    onCancel(){
      this.router.navigate(['/feedback-list']);
    }

  ngOnInit() {
  }

}
