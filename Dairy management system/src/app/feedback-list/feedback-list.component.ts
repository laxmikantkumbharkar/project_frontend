import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FeedbackService } from '../feedback.service';

@Component({
  selector: 'feedback-list',
  templateUrl: './feedback-list.component.html',
  styleUrls: ['./feedback-list.component.css']
})
export class FeedbackListComponent implements OnInit {

  Feedback = [];
  Name = '';
  Address = ''; 
  Taluka = '';
  District = ''; 
  Mobile = ''; 
  Email = '';
  fb = '';  
  Response = '';

  constructor(
    private router: Router,
    private feedbackService: FeedbackService ) { 
        this.refreshFeedbackList();
  }

  refreshFeedbackList(){
    this.feedbackService
        .getFeedback()
        .subscribe( response => {
          const result = response.json();
           console.log(result);
           this.Feedback = result.data;
        });
  }  

    onDetails(feedback){
    this.router.navigate(['/feedback-details'], { queryParams : { Name:feedback.Name}});
    }

    // onDelete(feedback){
    //     const answer = confirm('Are you sure want to delete '+ feedback.Name + ' ?');
    //     if(answer){
    //         this.feedbackService
    //             .deleteFeedback(feedback.Name)
    //             .subscribe( response => {
    //                 const result = response.json();
    //                 console.log(result);

    //                 this.refreshFeedbackList();
    //             });
    //     }
    // }

    onUpdate(feedback){
        this.router.navigate(['feedback-reply'],{ queryParams: { Name: feedback.Name }});
    }
    
    ngOnInit() {
    }

    }
