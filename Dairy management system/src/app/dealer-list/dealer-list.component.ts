import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DealerService } from '../dealer.service';

@Component({
  selector: 'dealer-list',
  templateUrl: './dealer-list.component.html',
  styleUrls: ['./dealer-list.component.css']
})
export class DealerListComponent implements OnInit {

  Dealers = [];
  Dname = '';
  Daddress = ''; 
  Dphone = null;
  Agency_name = ''; 
  Dusername = '';
  Dpassword = ''; 
  Age = null;
  Gender = '';
  Demail = '';
  Dthumbnail = '';

  constructor(
    private router: Router,
    private dealerService: DealerService) {
        this.refreshDealerList();
     }

     refreshDealerList(){
        this.dealerService
            .getDealer()
            .subscribe(response =>{
                const result = response.json();
                console.log(result);
                  this.Dealers = result.data;
            });
     }

     onDetails(dealer){
        this.router.navigate(['/dealer-details'],{ queryParams: { Id:dealer.Id}});
     }

     onDelete(dealer){
        const answer = confirm('Are you sure want to delete '+ dealer.Id + ' ?');
        if(answer){
          this.dealerService
              .deleteDealer(dealer.Id)
              .subscribe(response =>{
                  const result = response.json();
                  console.log(result);
                  this.refreshDealerList();
              });
        }
     }

     onUpdate(dealer){
        this.router.navigate(['/update-dealer'],{ queryParams : { Id:dealer.Id}});
     }

  ngOnInit() {
  }

}
