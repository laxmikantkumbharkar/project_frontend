import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  selectedCustomer = {};
  Customer = [];
  Id = null;
  Cname = ''; 
  Carea = '';
  Pin = null;
  Phone = null;
  Cemail = '';
  Cusername = '';
  Cpassword = '';
  Cgender = '';
  Cthumbnail = '';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService  ) {
      this.refreshProductList(); 
        this.activatedRoute.queryParams.subscribe(params => {
          const Id = params['Id'];

          this.customerService
              .getCustomerDetails(Id)
              .subscribe(response => {
                const result = response.json();
                this.selectedCustomer = result.data;
              });
        });
    }

    refreshProductList(){
      this.customerService
          .getCustomers()
          .subscribe(response => {
            const result = response.json();
            console.log(result);
            this.Customer=result.data;
          });
    }

    onCancel(){
      return this.router.navigate(['/customer-list']);
    }

  ngOnInit() {
  }

}
