import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MilkRateListComponent } from './milk-rate-list.component';

describe('MilkRateListComponent', () => {
  let component: MilkRateListComponent;
  let fixture: ComponentFixture<MilkRateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilkRateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilkRateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
