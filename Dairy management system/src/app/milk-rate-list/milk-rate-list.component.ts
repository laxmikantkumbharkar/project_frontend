import { Component, OnInit } from '@angular/core';
import { MilkRateService } from '../milkrate.service';
import { Router } from '@angular/router'

@Component({
  selector: 'milk-rate-list',
  templateUrl: './milk-rate-list.component.html',
  styleUrls: ['./milk-rate-list.component.css']
})
export class MilkRateListComponent implements OnInit {

  Milk_rate = [];
  Fat=null;
  Buffalo=null;
  Cow=null;
  Updated_on=Date;
  Sr_No=null;

  constructor(
    private router: Router,
    private milkrateService: MilkRateService) { 
      this.refreshMilkRateList();
    }

    refreshMilkRateList(){
      this.milkrateService
          .getMilkrate()
          .subscribe(response =>{
              const result = response.json();
              console.log(result);
              this.Milk_rate= result.data;
          });
    }

    onDeleteMilkRate(milkrate){
      const answer = confirm('Are you sure want to delete '+milkrate.Sr_No+ ' ?');
      if(answer){
        this.milkrateService
            .deleteMilkrate(milkrate.Sr_No)
            .subscribe(response =>{
                const result= response.json();
                console.log(result);
                this.refreshMilkRateList();
            });
      }

    }

  ngOnInit() {
  }

}
