import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedbackService } from '../feedback.service';

@Component({
  selector: 'feedback-details',
  templateUrl: './feedback-details.component.html',
  styleUrls: ['./feedback-details.component.css']
})
export class FeedbackDetailsComponent implements OnInit {
  
  selectedFeedback = {};
  Feedback = [];
  Name = '';
  Address = '';
  Taluka = '';
  District = '';
  Mobile = '';
  Email = '';
  fb = '';
  date = '';
  Response = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private feedbackService: FeedbackService) { 
  
      this.refreshFeedbackList();  
      this.activatedRoute.queryParams.subscribe(params => {
        const Name = params['Name'];

        this.feedbackService
            .getFeedbackDetails(Name)
            .subscribe(response => {
              const result = response.json();
              this.selectedFeedback = result.data;
            });
      });
  }

  refreshFeedbackList(){
    this.feedbackService
        .getFeedback()
        .subscribe( response => {
          const result = response.json();
           console.log(result);
           this.selectedFeedback = result.data;
        });
  }  

  onCancel(){
    return this.router.navigate(['/feedback-list']);
  }

  ngOnInit() {
  }

}
