import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class CustomerService {
    url = 'http://localhost:9000/Customer';

    constructor(private http: Http){

    }

    getCustomers(){
        return this.http.get(this.url);
    }

    addCustomers(
        Cname:string,Carea:string,Pin:number,
        Phone:string,Cemail:string,Cusername:string,
        Cpassword:string,Cgender:string,selectedFile: any)
        {

        const body= new FormData();
        body.append('Cname',Cname); 
        body.append('Carea',Carea); 
        body.append('Pin',''+ Pin);
        body.append('Phone',Phone); 
        body.append('Cemail',Cemail); 
        body.append('Cusername',Cusername);
        body.append('Cpassword',Cpassword); 
        body.append('Cgender',Cgender);
        body.append('Cthumbnail',selectedFile);

        const header = new Headers({'Content-Type': 'application/json'});
        const requestOption = new RequestOptions({headers: header});
        return this.http.post(this.url,body,requestOption);
    }

    deleteCustomer(Id: number){
        return this.http.delete(this.url + '/' + Id);
    }

    getCustomerDetails(Id: number){
        return this.http.get(this.url + '/' + Id);
    }

    updateCustomer(
        Cname:string,Carea:string,Pin:number,
        Phone:string,Cemail:string,Cusername:string,
        Cpassword:string,Cgender:string,Cthumbnail:string
    ){
        const body= new FormData();
        body.append('Cname',Cname); 
        body.append('Carea',Carea); 
        body.append('Pin',''+ Pin);
        body.append('Phone',Phone); 
        body.append('Cemail',Cemail); 
        body.append('Cusername',Cusername);
        body.append('Cpassword',Cpassword); 
        body.append('Cgender',Cgender);
        body.append('Cthumbnail',Cthumbnail);

        const header = new Headers({'Content-Type': 'application/json'});
        const requestOption = new RequestOptions({ headers: header});

        return this.http.put(this.url, body, requestOption);
    }
}