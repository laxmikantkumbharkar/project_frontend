import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DealerService } from '../dealer.service';

@Component({
  selector: 'add-dealer',
  templateUrl: './add-dealer.component.html',
  styleUrls: ['./add-dealer.component.css']
})
export class AddDealerComponent implements OnInit {
  
  Dname = '';
  Daddress = '';
  Dphone = null;
  Agency_name = ''; 
  Dusername = '';
  Dpassword = ''; 
  Age = null;
  Gender = '';
  Demail = '';
  Dthumnail = '';
  constructor(
    private router: Router,
    private dealerService: DealerService
  ) { }

  onAdd(){
    this.dealerService
        .addDealer(this.Dname, this.Daddress, this.Dphone,
                  this.Agency_name, this.Dusername, this.Dpassword,
                  this.Age, this.Gender, this.Demail,this.Dthumnail)
        .subscribe(response =>{
          console.log(response);

          this.router.navigate(['/dealer-list']);
        });
  }

  onCancel(){
    return this.router.navigate(['/dealer-list']);
  }

  ngOnInit() {
  }

}
